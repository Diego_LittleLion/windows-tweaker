# This script disable the Windows 11 widgets

# Author: Diego LittleLion
# https://gitlab.com/Diego_LittleLion/repair-windows-integrity

Import-Module -Name .\modules\registry.psm1

Set-RegistryValue -Path "HKLM:\SOFTWARE\Microsoft\PolicyManager\default\NewsAndInterests" -Name "value" -Value 0

Set-RegistryValue -Path "HKLM:\SOFTWARE\Policies\Microsoft\Dsh" -Name "AllowNewsAndInterests" -Value 0