# Remove Windows Bloatware

# Author: Diego LittleLion
# https://gitlab.com/Diego_LittleLion/repair-windows-integrity

# Show some information and ask the user if start with the process
Write-Output "This program will remove and uninstall all the bloatware from the system."
$start = Read-Host -Prompt 'Start? (y/n)'

# If user press 'y', start with the remove, if not we close the program
if ($start -ne 'y') {
    Write-Output "You have chosen 'n' or an option other than 'y', exiting without making any changes..."
    exit
}

# Get the bloatware program list
$bloatware = Get-Content -Path ".\data\bloatware.txt"

# Remove the items
foreach ($item in $bloatware) {
    Write-Output "Trying to uninstall $($item)"
    Get-AppxPackage *$item* | Remove-AppxPackage
}

# Get-WindowsCapability -Online | where state -eq 'Installed'
Write-Output "Disable Windows PowerShell V2 Root"
Disable-WindowsOptionalFeature -Online -FeatureName MicrosoftWindowsPowerShellV2Root -NoRestart | Out-Null
Write-Output "Disable SMBDirect"
Disable-WindowsOptionalFeature -Online -FeatureName SMBDirect -NoRestart | Out-Null
Write-Output "Disable MediaPlayback"
Disable-WindowsOptionalFeature -Online -FeatureName MediaPlayback -NoRestart | Out-Null
Write-Output "Remove Windows PowerShell ISE"
Remove-WindowsCapability -Online -Name Microsoft.Windows.PowerShell.ISE~~~~0.0.1.0 | Out-Null
Write-Output "Remove StepsRecorder"
Remove-WindowsCapability -Online -Name App.StepsRecorder~~~~0.0.1.0 | Out-Null
Write-Output "Remove Internet Explorer"
Remove-WindowsCapability -Online -Name Browser.InternetExplorer~~~~0.0.11.0 | Out-Null
Write-Output "Remove Extended Wallpapers"
Remove-WindowsCapability -Online -Name Microsoft.Wallpapers.Extended~~~~0.0.1.0 | Out-Null
