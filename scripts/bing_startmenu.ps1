# This script disable the bing search on startmenu on Windows 11

# Author: Diego LittleLion
# https://gitlab.com/Diego_LittleLion/repair-windows-integrity

$registryPath = "HKCU:\SOFTWARE\Policies\Microsoft\Windows\Explorer"
$registryKey = "DisableSearchBoxSuggestions"

if (-not (Test-Path $registryPath)) {
    New-Item -Path $registryPath | Out-Null
}
if (-not (Get-ItemProperty -Path $registryPath -Name $registryKey -ErrorAction SilentlyContinue)) {
    Set-ItemProperty -Path $registryPath -Name $registryKey -Value 0
}

$registryValue = (Get-ItemProperty -Path $registryPath -Name $registryKey).$registryKey
if ($registryValue -eq 0) {
    $choice = Read-Host "Do you want to turn off the bing browser on startmenu? (y/n)"
    if ($choice -eq 'y') {
        Set-ItemProperty -Path $registryPath -Name $registryKey -Value 1
        Write-Host "The bing browser on startmenu has been turned off"
    }
    else {
        Write-Host "The bing browser on startmenu is still turned on"
    }
}
elseif ($registryValue -eq 1) {
    $choice = Read-Host "Do you want to turn on the bing browser on startmenu? (y/n)"
    if ($choice -eq 'y') {
        Set-ItemProperty -Path $registryPath -Name $registryKey -Value 0
        Write-Host "The bing browser on startmenu has been turned on"
    }
    else {
        Write-Host "The bing browser on startmenu is still turned off"
    }
}
else {
    Write-Host "Invalid value for the key"
}
