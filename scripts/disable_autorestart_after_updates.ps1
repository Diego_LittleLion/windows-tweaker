# This script install basic apps and settings

# Author: Diego LittleLion
# https://gitlab.com/Diego_LittleLion/repair-windows-integrity

Import-Module -Name .\modules\registry.psm1

Write-Output "Turning off autorestart after updates..."
Set-RegistryValue -Path "HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "NoAutoRebootWithLoggedOnUsers" -Value 1
