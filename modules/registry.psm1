function Set-RegistryValue {
    param (
        [parameter(Mandatory = $true)]
        [string]
        $Path,

        [parameter(Mandatory = $true)]
        [string]
        $Name,

        [parameter(Mandatory = $true)]
        $Value
    )
    if (-not (Test-Path $Path)) {
        New-Item -Path $Path | Out-Null
    }
    Set-ItemProperty -Path $Path -Name $Name -Value $Value | Out-Null
}
