# Disable Windows ads

# Author: Diego LitleLion
# https://gitlab.com/Diego_LittleLion/repair-windows-integrity

Import-Module -Name .\modules\registry.psm1

# Disable Windows ads
Set-RegistryValue -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\UserProfileEngagement' -Name ScoobeSystemSettingEnabled -Value 0
Set-RegistryValue -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager' -Name SubscribedContent-338389Enabled -Value 0
Set-RegistryValue -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager' -Name SubscribedContent-310093Enabled -Value 0
Set-RegistryValue -Path 'HKCU:\\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager' -Name SlientInstalledAppsEnabled -Value 0

# Disable using Advertising ID for Relevant Ads
Set-RegistryValue -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\AdvertisingInfo" -Name "Enabled" -Value 0 

# Disable personalized experiences with diagnostic data
Set-RegistryValue -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Privacy" -Name "TailoredExperiencesWithDiagnosticDataEnabled" -Value 0 

# Disable improving writing and typing recognition
Set-RegistryValue -Path "HKCU:\Software\Microsoft\Input\TIPC" -Name "Enabled" -Value 0 

# Disable personalized writing and typing recognition
Set-RegistryValue -Path "HKCU:\Software\Microsoft\InputPersonalization" -Name "RestrictImplicitInkCollection" -Value 1 
Set-RegistryValue -Path "HKCU:\Software\Microsoft\InputPersonalization" -Name "RestrictImplicitTextCollection" -Value 1 
Set-RegistryValue -Path "HKCU:\Software\Microsoft\InputPersonalization\TrainedDataStore" -Name "HarvestContacts" -Value 0 
Set-RegistryValue -Path "HKCU:\Software\Microsoft\Personalization\Settings" -Name "AcceptedPrivacyPolicy" -Value 0 

# Disable sending Diagnostic and Usage Data
Set-RegistryValue -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\DataCollection" -Name "AllowTelemetry" -Value 0 

# Disable Letting Windows improve Start and search results by tracking app launches
Set-RegistryValue -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "Start_TrackProgs" -Value 0 

# Disable Activity History
Set-RegistryValue -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\System" -Name "PublishUserActivities" -Value 0 

# Gamming
Set-RegistryValue -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\CloudExperienceHost\Intent\gaming -Name Intent -Value 0 

# Family
Set-RegistryValue -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\CloudExperienceHost\Intent\family -Name Intent -Value 0 

# Creative
Set-RegistryValue -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\CloudExperienceHost\Intent\creative -Name Intent -Value 0 

# School
Set-RegistryValue -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\CloudExperienceHost\Intent\schoolwork -Name Intent -Value 0 

# Entertaiment
Set-RegistryValue -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\CloudExperienceHost\Intent\entertainment -Name Intent -Value 0 

# Business
Set-RegistryValue -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\CloudExperienceHost\Intent\business -Name Intent -Value 0 

# Developer
Set-RegistryValue -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\CloudExperienceHost\Intent\developer -Name Intent -Value 0 