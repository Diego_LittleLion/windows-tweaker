# Upgrade all programs available on winget repository

# Autor: Diego LittleLion
# https://gitlab.com/Diego_LittleLion/repair-windows-integrity

# Updating programs
winget update --include-unknown --disable-interactivity --silent --all