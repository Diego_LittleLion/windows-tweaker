# This script install basic apps and settings

# Author: Diego LittleLion
# https://gitlab.com/Diego_LittleLion/repair-windows-integrity

Import-Module -Name .\modules\registry.psm1

Write-Output "Activating file extensions..."
Set-RegistryValue -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "HideFileExt" -Value 0

Write-Output "Installing VLC..."
winget install VideoLAN.VLC

Write-Output "Installing WinRaR..."
winget install RarLab.Winrar
