# What does this project do?
The purpose of this project is to have a pack of tools to tweak your Windows system.

# What i need to run it?
1. [Required] Have the '[app installer](https://apps.microsoft.com/store/detail/app-installer/9NBLGGH4NNS1)' app installed/updated.
2. Preferably, have the latest version of Windows.
3. Preferably, have all the applications in the Microsoft store updated.

# Instructions
Double click on windows-tweaker.bat file and click on "yes" on the UAC.

# What apps are going to be uninstalled if i run the debloat option?
Apps like Spotify, Onedrive, Weather, Disney, People, Paint 3D, Minecraft, Skype, Maps...