@ECHO off

REM Author: Diego LitleLion
REM https://gitlab.com/Diego_LittleLion/windows-tweaker

@setlocal enableextensions
@cd /d "%~dp0"

REM Checking that it has been run as administrator
net session >nul 2>&1
if %errorLevel% neq 0 (powershell start -verb runas '%0' am_admin & exit /b)

REM Show menu
powershell.exe -noprofile -executionpolicy bypass -file scripts\menu.ps1