# This script activate the verbose on Windows 11

# Author: Diego LittleLion
# https://gitlab.com/Diego_LittleLion/repair-windows-integrity

Import-Module -Name .\modules\registry.psm1

Set-RegistryValue -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name "verbosestatus" -Value 1