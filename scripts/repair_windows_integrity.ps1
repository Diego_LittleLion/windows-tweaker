# This Script checks and repairs the integrity of Windows

# Author: Diego LittleLion
# https://gitlab.com/Diego_LittleLion/repair-windows-integrity

Write-Output "Stopping Windows Update service..."
Stop-Service wuauserv
Write-Output "Windows Update service stopped."

Write-Output "Clearing the Windows Update cache..."
Remove-Item -Path C:\Windows\SoftwareDistribution\Download -Force -Recurse 2>$null
Write-Output "Windows Update cache cleared."

Write-Output "Starting Windows Update service..."
Start-Service wuauserv
Write-Output "Windows Update service started."

Write-Output "Deleting temporary files..."
Remove-Item -Path $env:TEMP -Force -Recurse 2>$null
Write-Output "Deleted temporary files."

Write-Output "Checking and repairing the integrity of the operating system (this may take a moment)..."
# Check Windows integrity
sfc /scannow
# https://learn.microsoft.com/en-us/powershell/module/dism/repair-windowsimage?view=windowsserver2022-ps
Repair-WindowsImage -Online -RestoreHealth | Out-Null
Repair-WindowsImage -Online -StartComponentCleanup -ResetBase | Out-Null

$Host.UI.RawUI.FlushInputBuffer()
Write-Output "Task completed. Please restart your computer."