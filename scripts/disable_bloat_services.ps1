# Turn to manual startup a bunch of bloat services

# Author: Diego LitleLion
# https://gitlab.com/Diego_LittleLion/repair-windows-integrity

# Get the services list
$services = Get-Content -Path ".\data\services.txt"

# Turn to manual the services
foreach ($service in $services) {
    Write-Output "Turning $($service) to manual startup"
    Set-Service -Name $service -StartupType Manual 2> $NULL
}
