# Options
$menu = (
    ("Repair Windows Integrity", "scripts\repair_windows_integrity.ps1"),
    ("Upgrade all programs available on winget repository", "scripts\upgrade_programs_winget.ps1"),
    ("Remove Bloatware", "scripts\remove_windows_bloatware.ps1"),
    ("Basic apps and settings", "scripts\basic_apps_and_settings.ps1"),
    ("Disable ads and telemetry", "scripts\disable_ads_and_telemetry.ps1"),
    ("Turn off the bing search at start menu", "scripts\bing_startmenu.ps1"),
    ("Turn on the verbose on startup and shutdown", "scripts\turnon_verbose_startup.ps1"),
    ("Disable Windows 11 widgets for all users", "scripts\disable_windows_widgets.ps1"),
    ("[BETA] Disable autorestart after updates", "scripts\disable_autorestart_after_updates.ps1"),
    ("[BETA] Turn to manual startup a bunch of bloat services", "scripts\disable_bloat_services.ps1")
)

function showMenu() {
    Write-Output "----------------------------------"
    Write-Output "---       WINDOWS TWEAKER      ---"
    Write-Output "---    By: Diego LittleLion    ---"
    Write-Output "----------------------------------"
    for ($i = 0; $i -lt $menu.Length; $i++) {
        Write-Host "$($i + 1). $($menu[$i][0])"
    }
    Write-Host "0. Exit"

    # Ask for a option
    $chosenOption = Read-Host "Select a option"

    # Check option
    if ($chosenOption -match '^\d+$' -and [int]$chosenOption -ge 1 -and [int]$chosenOption -le $menu.Length) {
        $chosenOption = $menu[$chosenOption - 1]
        Invoke-Expression $chosenOption[1]
        pause
        Clear-Host
        showMenu
    }
    elseif ($chosenOption -match '^\d+$' -and $chosenOption -eq 0) {
        exit
    }
    else {
        Write-Host "Invalid option. Please select a valid number."
        showMenu
    }
}

# Show menu
showMenu